
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include "GraphBuilder.h"
#include <time.h>
#include "GraphCut.h"

int main()
{

		int imageWidth = 450;
		int imageHeight = 375;
		int graphSize = imageWidth*imageHeight;

		int fgSize = 0;
		int bgSize = 0;

		FILE *fp = fopen("D:\\Workspace\\souceP.txt", "r");
		fscanf(fp,"%d\n", &fgSize);

		int *soucePosX = (int*)malloc(sizeof(int)*fgSize);
		int *soucePosY = (int*)malloc(sizeof(int)*fgSize);

		for (int i = 0; i < fgSize; i++)
		{
			int X,Y = 0;
			fscanf(fp, "%d %d\n",&X,&Y);
			soucePosX[i] = X;
			soucePosY[i] = Y;
		}

		fclose(fp);

		fp = fopen("D:\\Workspace\\sinkP.txt", "r");
		fscanf(fp,"%d\n", &bgSize);

		int *sinkPosX = (int*)malloc(sizeof(int)*bgSize);
		int *sinkPosY = (int*)malloc(sizeof(int)*bgSize);

		for (int i = 0; i < bgSize; i++)
		{
			int X,Y = 0;
			fscanf(fp, "%d %d\n",&X,&Y);
			sinkPosX[i] = X;
			sinkPosY[i] = Y;
		}

		fclose(fp);

		struct color_t fg_clusters[64];
		struct color_t bg_clusters[64];

		fp = fopen("D:\\Workspace\\fg_cluster.txt", "r");
		for (int i = 0; i < 64; i++)
		{
			short R,G,B = 0;
			fscanf(fp, "%d %d %d\n",&R,&G,&B);
			fg_clusters[i].r = (unsigned char)R;
			fg_clusters[i].g = (unsigned char)G;
			fg_clusters[i].b = (unsigned char)B;
		}
		fclose(fp);

		fp = fopen("D:\\Workspace\\bg_cluster.txt", "r");
		for (int i = 0; i < 64; i++)
		{
			short R,G,B = 0;
			fscanf(fp, "%d %d %d\n",&R,&G,&B);
			bg_clusters[i].r = (unsigned char)R;
			bg_clusters[i].g = (unsigned char)G;
			bg_clusters[i].b = (unsigned char)B;
		}
		fclose(fp);

		unsigned char *datas = (unsigned char*)malloc(sizeof(unsigned char)*3*graphSize);
		fp = fopen("D:\\Workspace\\imgdata.txt", "r");
		for (int i = 0; i < graphSize; i++)
		{
			short R,G,B = 0;
			fscanf(fp, "%d %d %d\n",&R,&G,&B);
			datas[3*i] = (unsigned char)R;
			datas[3*i+1] = (unsigned char)G;
			datas[3*i+2] = (unsigned char)B;

		}
		fclose(fp);

		float lamdaA = 4,lamdaB = 13000;
		float buildTime = 0;

		//int leftWeight[12] = { 0,0,0,0,0,0,
		//					   0,0,3,3,4,1
		//					   };

		//int rightWeight[12] = {0,0,0,0,0,0,
		//					  0,3,3,4,1,0
		//					  };

		//int topWeight[12] = {0};
		//int bottomWeight[12] = {0};

		//int souce[12] = { 0,0,0,0,0,0,
		//				  0,3,9,5,6,2
		//			      
		//				};

		//int silk[12] = {0,0,0,0,0,0
		//				,0,10,2,1,8,9
		//				};

		struct device_t dev;
		dev.width_ex = imageWidth+2;
		dev.height_ex = imageHeight+2;
		int graphsize1 = dev.width_ex*dev.height_ex;

		CUDA_SAFE_CALL(cudaMalloc((void**)&(dev.g_leftWeight),sizeof(int)*graphsize1));
		CUDA_SAFE_CALL(cudaMalloc((void**)&(dev.g_rightWeight),sizeof(int)*graphsize1));
		CUDA_SAFE_CALL(cudaMalloc((void**)&(dev.g_topWeight),sizeof(int)*graphsize1));
		CUDA_SAFE_CALL(cudaMalloc((void**)&(dev.g_bottomWeight),sizeof(int)*graphsize1));
		CUDA_SAFE_CALL(cudaMalloc((void**)&(dev.g_datapos),sizeof(int)*graphsize1));
		CUDA_SAFE_CALL(cudaMalloc((void**)&(dev.g_datanei),sizeof(int)*graphsize1));


		//int *g_rightWeight = NULL;
		//int *g_topWeight = NULL;
		//int *g_bottomWeight = NULL;
		//int *g_datapos = NULL;
		//int *g_datanei = NULL;

		//CUDA_SAFE_CALL( cudaMemcpy( g_leftWeight, leftWeight, sizeof( int ) * 21 , cudaMemcpyHostToDevice   ) ) ;
		//CUDA_SAFE_CALL( cudaMemcpy( g_rightWeight, rightWeight, sizeof( int ) * 21 , cudaMemcpyHostToDevice   ) ) ;
		//CUDA_SAFE_CALL( cudaMemcpy( g_topWeight, topWeight, sizeof( int ) * 21 , cudaMemcpyHostToDevice   ) ) ;
		//CUDA_SAFE_CALL( cudaMemcpy( g_bottomWeight, bottomWeight, sizeof( int ) * 21 , cudaMemcpyHostToDevice   ) ) ;
		//CUDA_SAFE_CALL( cudaMemcpy( g_datapos, souce, sizeof( int ) * 21 , cudaMemcpyHostToDevice   ) ) ;
		//CUDA_SAFE_CALL( cudaMemcpy( g_datanei, silk, sizeof( int ) * 21 , cudaMemcpyHostToDevice   ) ) ;

		 GlobalWrapper gw =
			GC_Init(imageWidth+2, imageHeight+2, 0);

		clock_t time1 = clock();
		BuildGraph(dev,soucePosX,soucePosY,fgSize,
			sinkPosX,sinkPosY,bgSize,fg_clusters,bg_clusters,datas,buildTime);
		clock_t time2 = clock();
		printf("Time1:%lf\n",(double)(time2-time1)/CLOCKS_PER_SEC );

		GC_InitWeight(gw,dev.g_datapos ,
			dev.g_datanei, dev.g_topWeight,
			dev.g_bottomWeight, dev.g_leftWeight, dev.g_rightWeight);

		int * label = (int *) malloc(sizeof(int) * graphsize1);
		time1 = clock();
		GC_Optimize(gw,label);
		time2 = clock();
		printf("Time:%lf\n",(double)(time2-time1)/CLOCKS_PER_SEC );
		//fp = fopen("D:\\Workspace\\label.txt", "w");

		//int count = 0;
		//for (int i = 1; i < imageHeight+1; i++)
		//{
		//	for (int j = 1; j < imageWidth+1; j++)
		//	{
		//		int index = i*dev.width_ex+j;
		//		if (label[index] == 1)
		//		{
		//			//fprintf(fp,"1\n");

		//		}else
		//		{
		//			//fprintf(fp,"0\n");
		//		}
		//	}

		//}

		//fclose(fp);

		free(datas);
		free(soucePosX);
		free(soucePosY);
		free(sinkPosX);
		free(sinkPosY);

		free(label);
		getchar();
    return 1;
}
