Based on original open source Push Relabel library implemented by bruno, this library is modified for segmenting the stereo image by using the algorithm of StereoCut[1].

original library for single image: https://github.com/bfiss/gc-fg

References
[1] B. L. Price, S. Cohen, StereoCut: Consistent Interactive Object Selection in Stereo Image Pairs, in IEEE Inter- national Conference on Computer Vision, pp. 1148-1155, 2011.

