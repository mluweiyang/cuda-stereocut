#include "GraphBuilder.h"
#include "cuda_runtime.h"
#include "helper_timer.h"
#include "device_launch_parameters.h"

#define COLORCLUSTERS 64
#define DATABLOCKSIZE 32
#define INFINITY 10000000000

#define LAMDAA 1.0
#define LAMDAB 13000.0

/*************************************************
* CUDA Functions for Build Graph                **
* ***********************************************/

__host__ __device__ inline static
	float ColorDistanceL2(struct color_t color1,struct color_t color2)
{
    int i;
    float distance = 0;

	int r = color1.r - color2.r;
	int g = color1.g - color2.g;
	int b = color1.b - color2.b;

	distance =(float) r*r + g*g + b*b;
    return(distance);
}

__global__ void
kernel_sampleSouceNode(int *g_excessFlow,int *g_sink_weight, int *posX,int *posY,int size,int width)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < size)
	{
		int pX = posX[index];
		int pY = posY[index];


		int realIndex =  (pY+1)*width+(pX+1);
		g_excessFlow[realIndex] = INFINITY;
		g_sink_weight[realIndex] = 0;
	}

}

__global__ void
kernel_sampleSilkNode(int *g_sink_weight,int *g_excessFlow, int *posX, int *posY,int size,int width)
{

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < size)
	{
		int pX = posX[index];
		int pY = posY[index];


		int realIndex =  (pY+1)*width+(pX+1);

		g_sink_weight[realIndex] = INFINITY;
		g_excessFlow[realIndex] = 0;
	}
}

__global__ void
kernel_buildgraph(int *g_left_weight, int *g_right_weight, int *g_down_weight, int *g_up_weight,
		int *g_sink_weight, int *g_excessFlow,int width, int height, int width1, int rows1,
	    struct color_t *fg_centrids,struct color_t *bg_centrids,unsigned char* imgdata,
		int *lamdaA,int *lamdaB
		)
{
	int x1 = threadIdx.x ;
	int y1 = threadIdx.y ;
	int x  = blockIdx.x * blockDim.x + x1;
	int y  = blockIdx.y * blockDim.y + y1;

	//Init shared memroy and copy data into it
	//Copy Clusters
	__shared__ struct color_t shared_fg_centrids[64];
	__shared__ struct color_t shared_bg_centrids[64];

	//Block size is 33x33 = 1089
	__shared__ struct color_t shared_right_adjecent[32*32];
	__shared__ struct color_t shared_bottom_adjecent[32*32];

	int A = (*lamdaA);
	int B = (*lamdaA);

	struct color_t currentPixelColor;
	int rvalue = 0;
	int gvalue = 0;
	int bvalue = 0;

	if (x < width && y < height)
	{
		int imgWidthOffset = (24*width+31)/32 * 4;
		long imgdataOffset = (height-1-y)*(imgWidthOffset) + x*3;
		//long imgdataOffset = y*imgWidthOffset + 3*x;

		int realIndex = (y+1)*width1+(x+1);
		int block_thid = y1*32 + x1;

		struct color_t rightPixelColor;
		struct color_t bottomPixelColor;

		currentPixelColor.r = imgdata[imgdataOffset+2];
		currentPixelColor.g = imgdata[imgdataOffset+1];
		currentPixelColor.b = imgdata[imgdataOffset];

		rvalue = currentPixelColor.r;
		gvalue = currentPixelColor.g;
		bvalue = currentPixelColor.b;

		if (x1 != 0)
			shared_right_adjecent[block_thid-1] = currentPixelColor;
		if (y1 != 0)
			shared_bottom_adjecent[block_thid-32] = currentPixelColor;

		if (x1 == 31){			
			rightPixelColor.r = imgdata[imgdataOffset+5];
			rightPixelColor.g = imgdata[imgdataOffset+4];
			rightPixelColor.b = imgdata[imgdataOffset+3];
		}

		if (y1 == 31)
		{
			bottomPixelColor.r = imgdata[imgdataOffset-imgWidthOffset+2];
			bottomPixelColor.g = imgdata[imgdataOffset-imgWidthOffset+1];
			bottomPixelColor.b = imgdata[imgdataOffset-imgWidthOffset+0];
		}

		__syncthreads();

		if(block_thid < 64){
			shared_fg_centrids[block_thid] = fg_centrids[block_thid]; 
			shared_bg_centrids[block_thid] = bg_centrids[block_thid]; 
		}
		__syncthreads();

		//Add terminal node
		if (g_excessFlow[realIndex] == 0 && g_sink_weight[realIndex] == 0)
		{
			//Build Terminal Node
			float minfgDistance = INFINITY;
			float distance = 0;
			int rDis,gDis,bDis;

			for (int i = 0; i < 64; i++)
			{
				rDis = rvalue - shared_fg_centrids[i].r;
				gDis = gvalue - shared_fg_centrids[i].g;
				bDis = bvalue - shared_fg_centrids[i].b;

				 distance = (int)(rDis*rDis+gDis*gDis+bDis*bDis);
				(distance < minfgDistance)? minfgDistance = distance:minfgDistance;
			}

			minfgDistance = sqrt(minfgDistance);

			__syncthreads();

			float minbgDistance = INFINITY;
		    distance = 0;
			for (int i = 0; i < 64; i++)
			{
				rDis = rvalue - shared_bg_centrids[i].r;
				gDis = gvalue - shared_bg_centrids[i].g;
				bDis = bvalue - shared_bg_centrids[i].b;

				distance =  (int)(rDis*rDis+gDis*gDis+bDis*bDis);
				(distance < minbgDistance)? minbgDistance = distance:minbgDistance;
			}
			minbgDistance = sqrt(minbgDistance);
			float distanceNorm = minfgDistance + minbgDistance;

			float sEnergy = 0,dEnergy = 0;
		
			(distanceNorm != 0)?sEnergy = minbgDistance/distanceNorm:0;
			(distanceNorm != 0)?dEnergy = minfgDistance/distanceNorm:0;
		
			g_excessFlow[realIndex] += (*lamdaA)*sEnergy;
			g_sink_weight[realIndex] += (*lamdaA)*dEnergy;

		}

		__syncthreads();

		//Start to build up graph
		//Build Neiborhood
		(x1 != 31)?rightPixelColor = shared_right_adjecent[block_thid]:rightPixelColor;
		(y1 != 31)?bottomPixelColor = shared_bottom_adjecent[block_thid]:bottomPixelColor;
		
		if (x != width-1)
		{
			int rightEnergy =  (int)(*lamdaB)/(1.0+sqrt(ColorDistanceL2(rightPixelColor,currentPixelColor)));
			g_right_weight[realIndex] += rightEnergy;
			g_left_weight[realIndex+1] += rightEnergy;
		}

		if (y != height-1)
		{
			int downEnergy =  (int)(*lamdaB)/(1.0+sqrt(ColorDistanceL2(bottomPixelColor,currentPixelColor)));
			g_down_weight[realIndex] += downEnergy;
			g_up_weight[realIndex+width1] += downEnergy;
		}

	}

};

//Stereo Graph
// || Image 1 | Image 2||
//
__global__ void
kernel_buildstereograph(int *g_left_weight, int *g_right_weight, int *g_down_weight, int *g_up_weight,
		int *g_sink_weight, int *g_excessFlow,int *g_corres_weight,
		int width, int height,	//height is half of real image in memory
		int imageWidth,int imageHeight,
		int doubleWidth,
		int width1, int rows1,
	    struct color_t *fg_centrids,struct color_t *bg_centrids,
		int *fg_depth_centrids,int *bg_depth_centrids,
		int *leftDisparity,int *rightDispairty,
		unsigned char* imgdata,unsigned char*imgdata2,
		int *lamdaAa,int *lamdaAb,int *lamdaB,int *lamdaC
		)
{
	int x1 = threadIdx.x ;
	int y1 = threadIdx.y ;
	int x  = blockIdx.x * blockDim.x + x1;
	int y  = blockIdx.y * blockDim.y + y1;

	//Init shared memroy and copy data into it
	//Copy Clusters
	__shared__ struct color_t shared_fg_centrids[64];
	__shared__ struct color_t shared_bg_centrids[64];
	__shared__ int shared_bg_depth_centrids[32];
	__shared__ int shared_fg_depth_centrids[32];

	//Block size is 33x33 = 1089
	__shared__ struct color_t shared_right_adjecent[32*32];
	__shared__ struct color_t shared_bottom_adjecent[32*32];
2
	int B = (*lamdaB);
	int C = (*lamdaC);

	struct color_t currentPixelColor;
	int rvalue = 0;
	int gvalue = 0;
	int bvalue = 0;

	//Start build currentNeibor
	if (x < doubleWidth && y < height)
	{
		int imgWidthOffset = (24*width+31)/32 * 4;

		long imgdataOffset = (height-1-y)*(imgWidthOffset) + x*3;	//[Need modify]
		//long imgdataOffset = y*imgWidthOffset + 3*x;
		
		int orginalOffset;
		(x < width) ?orginalOffset = y*width+ x: y *width+(x-width);

		int realIndex = (y+1)*width1+(x+1);
		int block_thid = y1*32 + x1;
		
		//Load disparity
		int disparity;

		(x < width)? disparity = leftDisparity[orginalOffset]:rightDispairty[orginalOffset];
		bool haveCor;
		(x < width)? haveCor = (x - disparity > 0):(x + disparity < imageWidth);

		//Load Color
		struct color_t rightPixelColor;
		struct color_t bottomPixelColor;
		struct color_t corresColor;

		(x < width)? currentPixelColor.r = imgdata[imgdataOffset+2]: imgdata2[imgdataOffset+2];
		(x < width)? currentPixelColor.g = imgdata[imgdataOffset+1]: imgdata2[imgdataOffset+1];
		(x < width)? currentPixelColor.b = imgdata[imgdataOffset]:	imgdata2[imgdataOffset];

		rvalue = currentPixelColor.r;
		gvalue = currentPixelColor.g;
		bvalue = currentPixelColor.b;

		if (x1 != 0)
			shared_right_adjecent[block_thid-1] = currentPixelColor;
		if (y1 != 0)
			shared_bottom_adjecent[block_thid-32] = currentPixelColor;

		if (x1 == 31){			
			(x < width)?rightPixelColor.r = imgdata[imgdataOffset+5]:rightPixelColor.r = imgdata2[imgdataOffset+5];
			(x < width)?rightPixelColor.g = imgdata[imgdataOffset+4]:rightPixelColor.r = imgdata2[imgdataOffset+4];
			(x < width)?rightPixelColor.b = imgdata[imgdataOffset+3]:rightPixelColor.r = imgdata2[imgdataOffset+3];
		}

		if (y1 == 31)
		{
			(x < width)?bottomPixelColor.r = imgdata[imgdataOffset-imgWidthOffset+2]:rightPixelColor.r = imgdata2[imgdataOffset-imgWidthOffset+2];
			(x < width)?bottomPixelColor.g = imgdata[imgdataOffset-imgWidthOffset+1]:rightPixelColor.r = imgdata2[imgdataOffset-imgWidthOffset+1];
			(x < width)?bottomPixelColor.b = imgdata[imgdataOffset-imgWidthOffset]:rightPixelColor.r = imgdata2[imgdataOffset-imgWidthOffset];
		}

		__syncthreads();


		if(block_thid < 64){
			shared_fg_centrids[block_thid] = fg_centrids[block_thid]; 
			shared_bg_centrids[block_thid] = bg_centrids[block_thid]; 
		}

		if (block_thid < 32)
		{
			shared_bg_depth_centrids[block_thid] = bg_depth_centrids[block_thid];
			shared_fg_depth_centrids[block_thid] = fg_depth_centrids[block_thid];
		}
		__syncthreads();

		//Add terminal node
		if (g_excessFlow[realIndex] == 0 && g_sink_weight[realIndex] == 0)
		{
			//Build Terminal Node
			float minfgDistance = INFINITY;
			float distance = 0;
			float dDistance = 0;
			int rDis,gDis,bDis,dDis;

			//Color distance
			for (int i = 0; i < 64; i++)
			{
				rDis = rvalue - shared_fg_centrids[i].r;
				gDis = gvalue - shared_fg_centrids[i].g;
				bDis = bvalue - shared_fg_centrids[i].b;

				 distance = (int)(rDis*rDis+gDis*gDis+bDis*bDis);
				(distance < minfgDistance)? minfgDistance = distance:minfgDistance;
			}

			minfgDistance = sqrt(minfgDistance);

			__syncthreads();

			float minbgDistance = INFINITY;
		    distance = 0;
			for (int i = 0; i < 64; i++)
			{
				rDis = rvalue - shared_bg_centrids[i].r;
				gDis = gvalue - shared_bg_centrids[i].g;
				bDis = bvalue - shared_bg_centrids[i].b;

				distance =  (int)(rDis*rDis+gDis*gDis+bDis*bDis);
				(distance < minbgDistance)? minbgDistance = distance:minbgDistance;
			}
			minbgDistance = sqrt(minbgDistance);
			float distanceNorm = minfgDistance + minbgDistance;
			float depthDistanceNorm = minFgDepthDistance+minBgDepthDistance;

			float sEnergy1,dEnergy1;
			float sEnergy2,dEnergy2;

			(distanceNorm != 0)?sEnergy1 = minbgDistance/distanceNorm:0;
			(distanceNorm != 0)?dEnergy1 = minfgDistance/distanceNorm:0;
	
			(depthDistanceNorm != 0)?sEnergy2 = minBgDepthDistance/depthDistanceNorm:0;
			(depthDistanceNorm != 0)?dEnergy2 = minFgDepthDistance/depthDistanceNorm:0;

			g_excessFlow[realIndex] += (int)(sEnergy2);
			g_sink_weight[realIndex] += (int)(dEnergy2);

		}

		__syncthreads();

		//Start to build up graph
		//Build Neiborhood
		(x1 != 31)?rightPixelColor = shared_right_adjecent[block_thid]:rightPixelColor;
		(y1 != 31)?bottomPixelColor = shared_bottom_adjecent[block_thid]:bottomPixelColor;
		
		if (x != width-1)
		{
			int rightEnergy =  (int)(*lamdaB)/(1.0+sqrt(ColorDistanceL2(rightPixelColor,currentPixelColor)));
			g_right_weight[realIndex] += rightEnergy;
			g_left_weight[realIndex+1] += rightEnergy;
		}

		if (y != height-1)
		{
			int downEnergy =  (int)(*lamdaB)/(1.0+sqrt(ColorDistanceL2(bottomPixelColor,currentPixelColor)));
			g_down_weight[realIndex] += downEnergy;
			g_up_weight[realIndex+width1] += downEnergy;
		}

	}

};




void BuildGraph(struct device_t &dev,
				int *souceX,int* souceY, int souceNNZ,
				int *silkX,int* silkY, int silkNNZ,
			    struct color_t  *fg_centrids, struct color_t *bg_centrids,
				unsigned char *imageDatas,
				float &buildTime
				)
{
	int lamdaA = 100;
	int lamdaB = 2000;
	int *g_souceX,int* g_souceY;
	int *g_silkX,int* g_silkY;
	struct color_t *g_fg_centrids,*g_bg_centrids;
	unsigned char *imgData;

	int *g_lamdaA,*g_lamdaB;
	
	int imgWidth = dev.width_ex-2;
	int imgHeight = dev.height_ex-2;;
	int graphSize = dev.width_ex*dev.height_ex;

	//CUDA_SAFE_CALL( );
	CUDA_SAFE_CALL( cudaMalloc((void**)&g_souceY, sizeof(int) * souceNNZ ) );
	CUDA_SAFE_CALL( cudaMalloc((void**)&g_souceX, sizeof(int) * souceNNZ ) );

	CUDA_SAFE_CALL( cudaMalloc((void**)&g_silkX, sizeof(int) * silkNNZ ) );
	CUDA_SAFE_CALL( cudaMalloc((void**)&g_silkY, sizeof(int) * silkNNZ ) );

	CUDA_SAFE_CALL( cudaMalloc((void**)&g_lamdaA, sizeof(int)) );
	CUDA_SAFE_CALL( cudaMalloc((void**)&g_lamdaB, sizeof(int)) );

	CUDA_SAFE_CALL( cudaMalloc((void**)&g_fg_centrids, sizeof(struct color_t) * 64) );
	CUDA_SAFE_CALL( cudaMalloc((void**)&g_bg_centrids, sizeof(struct color_t) * 64) );
	CUDA_SAFE_CALL( cudaMalloc((void**)&imgData, sizeof(unsigned char) * 3*imgWidth*imgHeight) );

	CUDA_SAFE_CALL( cudaMemcpy( g_lamdaA, &lamdaA, sizeof( int ) , cudaMemcpyHostToDevice   ) ) ;
	CUDA_SAFE_CALL( cudaMemcpy( g_lamdaB, &lamdaB, sizeof( int ) , cudaMemcpyHostToDevice   ) ) ;

	CUDA_SAFE_CALL( cudaMemcpy( g_souceX, souceX, sizeof( int ) * souceNNZ , cudaMemcpyHostToDevice   ) ) ;
	CUDA_SAFE_CALL( cudaMemcpy( g_souceY, souceY, sizeof( int ) * souceNNZ , cudaMemcpyHostToDevice   ) ) ;
	CUDA_SAFE_CALL( cudaMemcpy( g_silkX, silkX, sizeof( int ) * silkNNZ , cudaMemcpyHostToDevice   ) ) ;
	CUDA_SAFE_CALL( cudaMemcpy( g_silkY, silkY, sizeof( int ) * silkNNZ , cudaMemcpyHostToDevice   ) ) ;
	CUDA_SAFE_CALL( cudaMemcpy( g_fg_centrids, fg_centrids, sizeof( struct color_t ) * 64 , cudaMemcpyHostToDevice   ) ) ;
	CUDA_SAFE_CALL( cudaMemcpy( g_bg_centrids, bg_centrids, sizeof( struct color_t ) * 64 , cudaMemcpyHostToDevice   ) ) ;
	CUDA_SAFE_CALL( cudaMemcpy( imgData, imageDatas, sizeof(unsigned char) * 3*imgWidth*imgHeight ,cudaMemcpyHostToDevice ) ) ;

	


	//Allocate edge weight
	CUDA_SAFE_CALL(cudaMemset(dev.g_datapos,0,sizeof(int)*graphSize));
	CUDA_SAFE_CALL(cudaMemset(dev.g_datanei,0,sizeof(int)*graphSize));
	CUDA_SAFE_CALL(cudaMemset(dev.g_leftWeight,0,sizeof(int)*graphSize));
	CUDA_SAFE_CALL(cudaMemset(dev.g_rightWeight,0,sizeof(int)*graphSize));
	CUDA_SAFE_CALL(cudaMemset(dev.g_topWeight,0,sizeof(int)*graphSize));
	CUDA_SAFE_CALL(cudaMemset(dev.g_bottomWeight,0,sizeof(int)*graphSize));

	/* Timer */
	cudaEvent_t start, stop; 
	cudaEventCreate(&start); 
	cudaEventCreate(&stop);	
	cudaEventRecord(start, 0);	
	
	

	int souceBlockSizeX = (int)(ceil((float)souceNNZ/32.0));
	kernel_sampleSouceNode<<<souceBlockSizeX,32>>>(dev.g_datapos,dev.g_datanei,g_souceX,g_souceY,souceNNZ,dev.width_ex);

	int sinkBlockSizeX = (int)(ceil((float)silkNNZ/32.0));
	kernel_sampleSilkNode<<<sinkBlockSizeX,32>>>(dev.g_datanei,dev.g_datapos,g_silkX,g_silkY,silkNNZ,dev.width_ex);

	int blockSizeY = (int)(ceil((int)imgHeight/32.0));
	int blockSizeX = (int)(ceil((int)imgWidth/32.0));
	dim3 blockScale(blockSizeX,blockSizeY,1);
	dim3 blockSize(32,32,1);


	kernel_buildgraph<<<blockScale,blockSize>>>(dev.g_leftWeight,dev.g_rightWeight,dev.g_bottomWeight,dev.g_topWeight,
												dev.g_datanei,dev.g_datapos,imgWidth,imgHeight,dev.width_ex,dev.height_ex
												,g_fg_centrids,g_bg_centrids,imgData,g_lamdaA,g_lamdaB);


	int *local = (int*)malloc(sizeof(int)*graphSize);
	CUDA_SAFE_CALL( cudaMemcpy(local, dev.g_datapos, sizeof( int ) * graphSize , cudaMemcpyDeviceToHost) ) ;
	for (int i = 0; i < 320; i++)
	{
		printf("%lf\n",local[i]);
	}

	int *local2 = (int*)malloc(sizeof(int)*graphSize);
	CUDA_SAFE_CALL( cudaMemcpy(local2, dev.g_datanei, sizeof( int ) * graphSize , cudaMemcpyDeviceToHost) ) ;
	for (int i = 0; i < graphSize; i++)
	{
		if (local2[i] < local[i])
		{
			int a = 0;
		}
	}

	cudaEventRecord(stop, 0);	
	cudaEventSynchronize(stop); 
	cudaEventElapsedTime(&buildTime, start, stop);

	CUDA_SAFE_CALL(cudaFree(g_souceX));
	CUDA_SAFE_CALL(cudaFree(g_souceY));
	CUDA_SAFE_CALL(cudaFree(g_silkX));
	CUDA_SAFE_CALL(cudaFree(g_silkY));
	CUDA_SAFE_CALL(cudaFree(g_fg_centrids));
	CUDA_SAFE_CALL(cudaFree(g_bg_centrids));
	CUDA_SAFE_CALL(cudaFree(imgData));
	
}
