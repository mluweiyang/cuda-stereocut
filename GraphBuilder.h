#include <stdio.h>
#include "GraphCut.h"

struct color_t{
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;
};

//For single image		
struct device_t
{

	int *g_leftWeight;
	int *g_rightWeight;
	int *g_topWeight;
	int *g_bottomWeight;
	int *g_datapos;
	int *g_datanei;

	int width_ex;
	int height_ex;
};

void BuildGraph(struct device_t &dev,
				int *souceX,int* souceY, int souceNNZ,
				int *silkX,int* silkY, int silkNNZ,
			    struct color_t  *fg_centrids, struct color_t *bg_centrids,
				unsigned char *imageDatas,
				float &buildTime
				);

//For stereo image
struct device_stereo_t
{
	int *g_leftWeight;
	int *g_rightWeight;
	int *g_topWeight;
	int *g_bottomWeight;
	int *g_datapos;
	int *g_datanei;
	
	int *g_correspondWeight;
	int *disparity;

	int width_ex;
	int height_ex;
};

void BuildStereoGraph(struct device_t &dev,
				int *souceX,int* souceY, int souceNNZ,
				int *silkX,int* silkY, int silkNNZ,
			    struct color_t  *fg_centrids, struct color_t *bg_centrids,
				int *fg_depth_centrids,int *bg_depth_centrids,
				unsigned char *imageDatas,unsigned char *imgeDatas2,
				int *leftDisparity,int *rightDisparity,
				float &buildTime
				);